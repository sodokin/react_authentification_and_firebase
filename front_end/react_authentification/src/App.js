import { Routes, Route } from 'react-router-dom'
import  Home  from './pages/Home'
import NavBar from '../src/components/NavBar'
import SignUpModal from './components/signUpModal'
import SignInModal from './components/signInModal'
import PrivatePage from './pages/private/privatePage'
import ConnectHome from './pages/private/PrivateHome/ConnectHome'

function App() {
  return (
    <>
      <SignUpModal/>
      <SignInModal/>
      <NavBar/>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/auth-page' element={<PrivatePage/>}>
          <Route path='/auth-page/connect-home' element={<ConnectHome/>} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
