import React, { useContext } from 'react'
import { UserContext } from '../context/userContext'
import { Link } from 'react-router-dom'
import { signOut } from 'firebase/auth'
import { useNavigate } from 'react-router-dom'
import { auth } from '../firebase-config'


export default function NavBar() {
    const { toggleModals } = useContext(UserContext);
    const logOutNavigate = useNavigate();

    const logOut = async () => {
        try {
            await signOut(auth)
            logOutNavigate("/")
        } catch (error) {
            alert("Pour une raison inconnue, vous ne pouvez pas être déconnecté, veuillez verifier votre connexion internet et essayez à nouveau!")
        }
    }
    return (
        <nav className='navbar navbar-ligth bg-black px-4'>
            <Link to='/' className='navbar'>
                AuthJS
            </Link>
            <div>
                <button onClick={() => { toggleModals("signUp") }} className='btn btn-primary'>Sign Up</button>
                <button onClick={() => { toggleModals("signIn") }} className='btn btn-primary ms-2' >Sign In</button>
                <button onClick={logOut} className='btn btn-danger ms-2' >Log out</button>

            </div>
        </nav>
    )
}