import React, { useContext, useRef, useState } from 'react'
import { UserContext } from '../context/userContext'
import { useNavigate } from 'react-router-dom'

export default function SignUpModal() {
    const [validationMessage, setValidation] = useState("")
    const {modalState, toggleModals, signUp} = useContext(UserContext)
    console.log(signUp)

    const inputs = useRef([])
    const formRef = useRef()
    const goToConnectPage = useNavigate();

    const addInputDatas = elt => {
        if (elt && ! inputs.current.includes(elt)) {
            inputs.current.push(elt)
        }
    }

    const handleFormSubmit = async (event) => {
        event.preventDefault()
        if ((inputs.current[1].value.length || inputs.current[2].value.length) < 6){
            setValidation("6 caractères minimum");
            return;
        } else if (inputs.current[1].value !== inputs.current[2].value){
            setValidation("Les mots de passe doivent être identiques!")
            return;
        }

        try {
            // const credentials = 
            await signUp(
                inputs.current[0].value,
                inputs.current[1].value,
            )
            formRef.current.reset()
            setValidation("")
            goToConnectPage("/auth-page/connect-home")
            toggleModals("close")

            // console.log(credentials)
        } catch (error) {
            if (error.code === "auth/invalid-email") {
                setValidation("Le format de votre addresse mail est invalide!")
            } else if (error.code === "auth/email-already-in-use") {
                setValidation("Cette addresse mail est déjà utitlisée!")
            }
        }


        console.log(inputs)
    }

    const closeModal = () => {
        setValidation("")
        toggleModals("close")
    }
    // console.log( modalState, toggleModals)

    return (
        <>
            {modalState.signUpModal && (
                <div className='position-fixed top-0 vw-100 vh-100'>

                    <div onClick={closeModal } className='w-100 h-100 bg-dark bg-opacity-75'>
                    </div>

                        <div className='position-absolute top-50 start-50 translate-middle' style={{minWidth: "400px"}}>
                            <div className='modal-dialog'>
                                <div className='modal-content'>
                                    <div className='modal-header'>
                                        <h5 className='modal-title'>Sign Up</h5>
                                        <button onClick={closeModal} className='btn-close'></button>  
                                    </div>
                                    <div className='modal-body'>
                                        <form ref={formRef} onSubmit={handleFormSubmit} className='sign-up-form'>
                                            <div className='mb-3'>
                                                <label htmlFor='signUpEmail' className='form-label' >Email adress</label>
                                                <input ref = { addInputDatas } name='email' id='signUpEmail' type="email" className='form-control' required ></input>
                                            </div>
                                            <div className='mb-3'>
                                                <label htmlFor='signUpPwd' className='form-label' >Password</label>
                                                <input ref = { addInputDatas } name='pwd' id='signUpPwd' type="password" className='form-control' required ></input>
                                            </div>
                                            <div className='mb-3'>
                                                <label htmlFor='repeatPwd' className='form-label' >Repeat Password</label>
                                                <input  ref = { addInputDatas } name='rpwd' id='repeatPwd' type="password" className='form-control' required ></input>
                                                <p className='text-danger mt-1'>{validationMessage}</p>
                                            </div>

                                            <button className='btn btn-primary' >Submit</button>
                                        </form>

                                    </div>

                                </div>

                            </div>
                        </div>

                </div>
            )}
        </>
    )
}
