import React, { useContext, useRef, useState } from 'react'
import { UserContext } from '../context/userContext'
import { useNavigate } from 'react-router-dom'


export default function SignInModal() {
    const [validationMessage, setValidation] = useState("")
    const {modalState, toggleModals, signIn} = useContext(UserContext)
    console.log(signIn)

    const goToConnectPage = useNavigate();
    const formRef = useRef()

    const inputs = useRef([])
    const addInputDatas = elt => {
        if (elt && ! inputs.current.includes(elt)) {
            inputs.current.push(elt)
        }
    }

    const handleFormSubmit = async (event) => {
        event.preventDefault()
        try {
            // const credentials = 
            await signIn(
                inputs.current[0].value,
                inputs.current[1].value,
            )
            formRef.current.reset()
            setValidation("")
            goToConnectPage("/auth-page/connect-home")
            toggleModals("close")

            // console.log(credentials)
        } catch (error) {
            setValidation("Zut, email et/ou mot de passe incorrect")
        }


        console.log(inputs)
    }

    const closeModal = () => {
        setValidation("")
        toggleModals("close")
    }
    // console.log( modalState, toggleModals)

    return (
        <>
            {modalState.signInModal && (
                <div className='position-fixed top-0 vw-100 vh-100'>

                    <div onClick={closeModal } className='w-100 h-100 bg-dark bg-opacity-75'>
                    </div>

                        <div className='position-absolute top-50 start-50 translate-middle' style={{minWidth: "400px"}}>
                            <div className='modal-dialog'>
                                <div className='modal-content'>
                                    <div className='modal-header'>
                                        <h5 className='modal-title'>Sign Up</h5>
                                        <button onClick={closeModal} className='btn-close'></button>  
                                    </div>
                                    <div className='modal-body'>
                                        <form ref={formRef} onSubmit={handleFormSubmit} className='sign-up-form'>
                                            <div className='mb-3'>
                                                <label htmlFor='signInEmail' className='form-label' >Email adress</label>
                                                <input ref = {addInputDatas} name='email' id='signInEmail' type="email" className='form-control' required ></input>
                                            </div>
                                            <div className='mb-3'>
                                                <label htmlFor='signInPwd' className='form-label' >Password</label>
                                                <input ref = {addInputDatas} name='pwd' id='signInPwd' type="password" className='form-control' required ></input>
                                                <p className='text-danger mt-1'>{validationMessage}</p>
                                            </div>
                                            <button className='btn btn-primary'>Submit</button>
                                        </form>

                                    </div>

                                </div>

                            </div>
                        </div>

                </div>
            )}
        </>
    )
}

