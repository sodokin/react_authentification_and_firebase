import React, { useContext } from 'react'
import { UserContext } from '../../context/userContext'
import { Outlet, useLocation, Navigate } from 'react-router-dom'

export default function PrivatePage() {
    const { currentUserState } = useContext(UserContext)
    console.log("PRIVATE", currentUserState)

    if (!currentUserState){
        return <Navigate to="/" />
    }

    return (
        <div className='container'>
            <Outlet/>
        </div>
    )
}
