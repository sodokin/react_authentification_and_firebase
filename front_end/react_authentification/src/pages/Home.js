import React, { useContext } from 'react'
import { UserContext } from '../context/userContext'

export default function Home() {
    const { currentUserState } = useContext(UserContext)
    return (
        <div className='container p-5'>
            <h1 className='display-3 '>
                {currentUserState ? "Bienvenue sur le site" : " Hi, sign up or sign in"}
            </h1>
        </div>
    )
}
