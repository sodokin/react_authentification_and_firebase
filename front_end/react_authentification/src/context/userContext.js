import { createContext, useState, useEffect } from "react";
import { signInWithEmailAndPassword, createUserWithEmailAndPassword, onAuthStateChanged } from "firebase/auth"
import { auth } from "../firebase-config"

export const UserContext = createContext();

export function UserContextProvider(props) {
    const signUp  = (email, password) => createUserWithEmailAndPassword(auth, email, password);
    const signIn  = (email, password) => signInWithEmailAndPassword(auth, email, password);


    const [currentUserState, setCurrentUserState] = useState();
    const [loadingDataState, setLoadingDataState] = useState(true);

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (currentUserState) => {
            setCurrentUserState(currentUserState)
            setLoadingDataState(false)
        })
        return unsubscribe;
    }, [])

    // Ici c'est la partie de la fenêtre modal
    const [modalState, setModalState] = useState({
        signUpModal: false,
        signInModal: false,
    })
    const toggleModals = modal => {
        if (modal === "signIn") {
            setModalState({
                signUpModal: false,
                signInModal: true
            })
        } else if (modal === "signUp"){
            setModalState({
                signUpModal: true,
                signInModal: false
            })

        }else {
            setModalState({
                signUpModal: false,
                signInModal: false
            })
        }
    }
    return (
        <UserContext.Provider value={{modalState, toggleModals, signUp, currentUserState, signIn}}>
            {!loadingDataState && props.children}
        </UserContext.Provider>
    )
}